package main;


import demSoLuongChuSoLe.DemSoLuongChuSoLe;
import demSoUocChan.DemSoUocChan;
import demSoUocNguyenDuong.DemSoUocNguyenDuong;
import kiemTraSoChinhPhuong.KiemTraSoChinhPhuong;
import lietKeSoUocNguyenDuong.LietKeSoUocNguyenDuong;
import lietKeUocSoLe.LietKeUocSoLe;
import nghichDao1DenN.NghichDao1DenN;
import soDaoNguocCuaN.SoDaoNguocCuaN;
import soDauTienCuaN.SoDauTienCuaN;
import soHoanThien.SoHoanThien;
import soLonNhatCuaN.SoLonNhatCuaN;
import soLuongChuCuaMotSoNguyenDuong.SoLuongChuCuaMotSoNguyenDuong;
import tichCacChuSoNguyenDuong.TichCacChuSoNguyenDuong;
import tichCacSoLeDuong.TichCacSoLeDuong;
import tichSoUocDuong.TichSoUocDuong;
import tichUocSoLe.TichUocSoLe;
import tongCacChuSoNguyenDuong.TongCacChuSoNguyenDuong;
import tongCacTichTu1DenN.TongCacTichTu1DenN;
import tongCanBac2Tu1DenCanN.TongCanBac2Tu1DenCanN;
import tongCanBac2Tu1GiaiThuaDenNGiaiThua.TongCanBac2Tu1GiaiThuaDenNGiaiThua;
import tongCanBac2TuNDenCan1.TongCanBac2TuNDenCan1;
import tongNCanBac2Cua2.TongNCanBac2Cua2;
import tongSoChanDuong.TongSoChanDuong;
import tongSoUocNguyenDuong.TongSoUocNguyenDuong;
import tongUocNguyenDuongNhoHonN.TongUocNguyenDuongNhoHonN;
import tongUocSoChan.TongUocSoChan;
import tongXmu2Ncong1Chia2Ncong1GiaiThua.TongXmu2Ncong1Chia2Ncong1GiaiThua;
import tongXmu2Ncong1Chia2NgiaiThua.TongXmu2Ncong1Chia2NgiaiThua;
import tongXmu2nCong1.TongXmu2nCong1;
import tongXmuNchia1DenN.TongXmuNchia1DenN;
import uocSoLeLonNhat.UocSoLeLonNhat;
import xMuNchiaNGiaiThua.XMuNchiaNGiaiThua;
import xmu2N.Xmu2N;

public class Main {
    public static void main(String[] args) {
        // Giải thuật tính tích từ 1 đến n
//        TichTu1DenN tichTu1DenN = new TichTu1DenN();
//        tichTu1DenN.mainTichTu1DenN();

        //Giải thuật tính tổng các số nghịch đảo
        // Xmu2N
//        NghichDao1DenN nghichDao1DenN =new NghichDao1DenN();
//        nghichDao1DenN.mainNghichDao1DenN();
//        TongCacTichTu1DenN tongCacTichTu1DenN=new TongCacTichTu1DenN();
//        tongCacTichTu1DenN.mainTongCacTichTu1DenN();
//        TongXmuNchia1DenN tongXmuNchia1DenN= new TongXmuNchia1DenN();
//        tongXmuNchia1DenN.mainTongXmuNchia1DenN();
//        XMuNchiaNGiaiThua xMuNchiaNGiaiThua=new XMuNchiaNGiaiThua();
//        xMuNchiaNGiaiThua.mainXMuNchiaNGiaiThua();
//        TongXmu2Ncong1Chia2NgiaiThua tongXmu2Ncong1Chia2NgiaiThua=new TongXmu2Ncong1Chia2NgiaiThua();
//        tongXmu2Ncong1Chia2NgiaiThua.mainTongXmu2Ncong1Chia2NgiaiThua();

//        TongXmu2Ncong1Chia2Ncong1GiaiThua tongXmu2Ncong1Chia2Ncong1GiaiThua=new TongXmu2Ncong1Chia2Ncong1GiaiThua();
//        tongXmu2Ncong1Chia2Ncong1GiaiThua.mainTongXmu2Ncong1Chia2Ncong1GiaiThua();
//        LietKeSoUocNguyenDuong lietKeSoUocNguyenDuong=new LietKeSoUocNguyenDuong();
//        lietKeSoUocNguyenDuong.mainLietKeSoUocNguyenDuong();
//        TongSoUocNguyenDuong tongSoUocNguyenDuong=new TongSoUocNguyenDuong();
//        tongSoUocNguyenDuong.mainTongSoUocNguyenDuong();
//        TichSoUocDuong tichSoUocDuong=new TichSoUocDuong();
//        tichSoUocDuong.mainTichSoUocDuong();
//        DemSoUocNguyenDuong demSoUocNguyenDuong=new DemSoUocNguyenDuong();
//        demSoUocNguyenDuong.mainDemSoUocNguyenDuong();
//        LietKeSoUocNguyenDuong lietKeSoUocNguyenDuong=new LietKeSoUocNguyenDuong();
//        lietKeSoUocNguyenDuong.mainLietKeSoUocNguyenDuong();
//        LietKeUocSoLe lietKeUocSoLe=new LietKeUocSoLe();
//        lietKeUocSoLe.mainLietKeUocSoLe();
//        TongUocSoChan tongUocSoChan=new TongUocSoChan();
//        tongUocSoChan.mainTongUocSoChan();
//        TichUocSoLe tichUocSoLe=new TichUocSoLe();
//        tichUocSoLe.mainTichUocSoLe();
//        DemSoUocChan demSoUocChan =new DemSoUocChan();
//        demSoUocChan.mainDemSoUocChan();
//        TongUocNguyenDuongNhoHonN tongUocNguyenDuongNhoHonN=new TongUocNguyenDuongNhoHonN();
//        tongUocNguyenDuongNhoHonN.mainTongUocNguyenDuongNhoHonN();
//        UocSoLeLonNhat uocSoLeLonNhat=new UocSoLeLonNhat();
//        uocSoLeLonNhat.mainUocSoLeLonNhat();
//        SoHoanThien soHoanThien=new SoHoanThien();
//        soHoanThien.mainSoHoanThien();
//        KiemTraSoChinhPhuong kiemTraSoChinhPhuong=new KiemTraSoChinhPhuong();
//        kiemTraSoChinhPhuong.mainkiemTraSoChinhPhuong();
//        TongNCanBac2Cua2 tongNCanBac2Cua2=new TongNCanBac2Cua2();
//        tongNCanBac2Cua2.mainTongNCanBac2Cua2();
//        TongCanBac2Tu1DenCanN tongCanBac2Tu1DenCanN =new TongCanBac2Tu1DenCanN();
//        tongCanBac2Tu1DenCanN.mainTongCanBac2Tu1DenCanN2();
//        TongCanBac2TuNDenCan1 tongCanBac2TuNDenCan1 =new TongCanBac2TuNDenCan1();
//        tongCanBac2TuNDenCan1.mainTongCanBac2TuNDenCan1();
//        TongCanBac2Tu1GiaiThuaDenNGiaiThua tongCanBac2Tu1GiaiThuaDenNGiaiThua=new TongCanBac2Tu1GiaiThuaDenNGiaiThua();
//        tongCanBac2Tu1GiaiThuaDenNGiaiThua.mainTongCanBac2Tu1GiaiThuaDenNGiaiThua();
//        SoLuongChuCuaMotSoNguyenDuong soLuongChuCuaMotSoNguyenDuong=new SoLuongChuCuaMotSoNguyenDuong();
//        soLuongChuCuaMotSoNguyenDuong.mainSoLuongChuCuaMotSoNguyenDuong();
//        TongCacChuSoNguyenDuong tongCacChuSoNguyenDuong=new TongCacChuSoNguyenDuong();
//        tongCacChuSoNguyenDuong.mainTongCacChuSoNguyenDuong();
//        TichCacChuSoNguyenDuong tichCacChuSoNguyenDuong=new TichCacChuSoNguyenDuong();
//        tichCacChuSoNguyenDuong.mainTichCacChuSoNguyenDuong();
//        DemSoLuongChuSoLe demSoLuongChuSoLe=new DemSoLuongChuSoLe();
//        demSoLuongChuSoLe.mainDemSoLuongChuSoLe();
//        TichCacSoLeDuong tichCacSoLeDuong=new TichCacSoLeDuong();
//        tichCacSoLeDuong.mainTichCacSoLeDuong();
//        TongSoChanDuong tongSoChanDuong=new TongSoChanDuong();
//        tongSoChanDuong.mainTongSoChanDuong();
//        SoDauTienCuaN soDauTienCuaN=new SoDauTienCuaN();
//        soDauTienCuaN.mainSoDauTienCuaN();
//        SoDaoNguocCuaN soDaoNguocCuaN=new SoDaoNguocCuaN();
//        soDaoNguocCuaN.mainSoDaoNguocCuaN();
        SoLonNhatCuaN soLonNhatCuaN=new SoLonNhatCuaN();
        soLonNhatCuaN.mainSoLonNhatCuaN();
    }
}


