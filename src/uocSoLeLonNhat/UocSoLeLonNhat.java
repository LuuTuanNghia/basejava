package uocSoLeLonNhat;

import java.util.Scanner;

public class UocSoLeLonNhat {
    public int uocSoLeLonNhat(int n) {
        int max=1;
        if (n % 2 != 0) {
            return n;
        } else {
            for (int i = 1; i < n; i+=2) {
                if (n % i == 0) {
                    max=i;
                }
            }
        }
        return max;
    }
    public void mainUocSoLeLonNhat() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("mời bạn nhập n :");
        int n = scanner.nextInt();
        System.out.println("uoc le lon nhat la : " + uocSoLeLonNhat(n));
    }
}
