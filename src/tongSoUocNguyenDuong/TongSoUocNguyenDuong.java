package tongSoUocNguyenDuong;

import java.util.Scanner;

public class TongSoUocNguyenDuong {
    public long tongSoUocNguyenDuong(int n){
        long s=0;
        int i=1;
        while (i<=n){
            if (n%i==0){
                s+=i;
            }
            i++;
        }
        return s;
    }
    public void mainTongSoUocNguyenDuong(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("mời bạn nhập n :");
        int n=scanner.nextInt();
        System.out.println("kq "+tongSoUocNguyenDuong(n));
    }
}
