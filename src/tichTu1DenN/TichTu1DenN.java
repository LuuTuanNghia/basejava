package tichTu1DenN;

import java.util.Scanner;

public class TichTu1DenN {
    public int tichTu1DenN(int n, int s) {
        for (int i = 1; i <= n; i++) {
            s *= i;
        }
        return s;
    }

    public void mainTichTu1DenN() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời bạn nhập n: ");
        int n = scanner.nextInt();
        int s = 1;
        System.out.println("kết quả là: " + tichTu1DenN(n, s));
    }
}
